import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import * as io from 'socket.io-client';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  private textInput: string;
  private messages: Array<any>;
  private socket;

  constructor (private appService: AppService) {
  }

  ngOnInit() {
    this.messages = [];
    try {
      this.socket = io('http://localhost:4000');
      this.socket.on('connection_refused', function() {
         console.log('Error');
      });
      this.socket.on('new-message', function (data) {
        this.messages.push(data);
      }.bind(this));
    } catch (e) {
    }
  }

  private enviar() {
    this.messages = [];
    const a = this.textInput.split('\n');
    console.log(a);
    this.appService.send(this.textInput).subscribe(
      next => {console.log(next); }
    );
  }
}
