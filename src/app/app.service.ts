import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AppService {

  constructor(private http: Http) { }

  public send(input: string) {
      return this.http.post('api/cube-sum', {'input': input})
        .map((res: Response) => res.json());
  }

}
