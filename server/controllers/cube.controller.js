var Cube = require('../classes/cube.class');

exports.init = function(N, operations){
      var cube = new Cube(5);
      console.log('init: '+N);
      var operationPromises = [];
      for (var i = 0; i < operations.length; i++) {
        var type = operations[i].split(' ')[0];
        if(type==='UPDATE'){
          var x = operations[i].split(' ')[1];
          var y = operations[i].split(' ')[2];
          var z = operations[i].split(' ')[3];
          var W = parseInt(operations[i].split(' ')[4]);
          console.log('UPDATE '+x+' '+y+' '+z+' '+W);
          // var p = this.update(x, y, z, W, cube);
          operationPromises.push(this.update(x, y, z, W, cube));
        }
        else if (type==='QUERY') {
          var x1 = operations[i].split(' ')[1];
          var y1 = operations[i].split(' ')[2];
          var z1 = operations[i].split(' ')[3];
          var x2 = operations[i].split(' ')[4];
          var y2 = operations[i].split(' ')[5];
          var z2 = operations[i].split(' ')[6];
          console.log('QUERY '+x1+' '+y1+' '+z1+' '+x2+' '+y2+' '+z2);
          // var p1 = {promise :this.sum(x1, y1, z1, x2, y2, z2, cube)};
          operationPromises.push(this.sum(x1, y1, z1, x2, y2, z2, cube));
        }
      }

      return Promise.all(operationPromises);
}

exports.sum = function(x1, y1, z1, x2, y2, z2, cube){
    // sum = cube.sum(x1, y1, z1, x2, y2, z2);
    // console.log(sum);
    return Promise.resolve(cube.sum(x1, y1, z1, x2, y2, z2));

}

exports.update = function(x, y, z, W, cube){
    return new Promise((resolve, reject) => {
      cube.update(x, y, z, W);
      resolve('update');
    });
}
