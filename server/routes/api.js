const express = require('express');
const router = express.Router();
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

var cubeService = require('../services/cube.service');

server.listen(4000);

// socket io
io.on('connection', function (socket) {
  console.log('User connected');
  socket.on('disconnect', function() {
    console.log('User disconnected');
  });
  socket.on('save-message', function (data) {
    console.log(data);
    io.emit('new-message', { message: data });
  });
});


/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

router.post('/cube-sum', (new cubeService(io)).sum);


module.exports = router;
