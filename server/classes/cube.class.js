class Cube {

  constructor(sideP) {
    this.updates = [];
    this.side = sideP;
    var r, x, y, z;
    for (r = [], x = 0; x < this.side; x++)
        for (r [x] = [], y = 0; y < this.side; y++)
             for (r [x][y] = [], z = 0; z < this.side; z++)
                  r [x][y][z] = 0;
    this.cube = r;
  }

  sum(x1, y1, z1, x2, y2, z2)
  {
    var r = 0;
    for (var i = 0; i < this.updates.length; i++) {
      var u = this.updates[i].split(',');
      // console.log(u);
      var x = u[0];
      var y = u[1];
      var z = u[2];
      var W = parseInt(u[3]);
      if(x>=x1 && x<=x2 && y>=y1 && y<=y2 && z>=z1 && z<=z2){
        r += W;
      }
    }
    // console.log(r);
    return r;
  }

  update(x, y, z, W){
    this.cube [x][y][z]=W;
    this.updates.push(x+','+y+','+z+','+W);
    // console.log(this.updates[this.updates.length-1]);
  }
}


module.exports = Cube;
