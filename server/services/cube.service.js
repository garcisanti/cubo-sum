var express = require('express');
var cubeController = require('../controllers/cube.controller');

function cubeService(io) {

this.sum = function(req, res){
  var b = req.body.input;
  var lines = b.split('\n');
  var pos = 0;
  var test = parseInt(lines[0]);
  pos++;
  var testPromises = [];
  while(test>0)
  {
    console.log('TEST: '+lines[pos]);
    var N = parseInt(lines[pos].split(' ')[0]);
    var M = parseInt(lines[pos].split(' ')[1]);
    pos++;
    var operations = lines.slice(pos, pos+M);
    var operationPromises = [];
    var p = cubeController.init(N, operations)
    .then(values => {
      // console.log('values: '+values);
      for (var i = 0; i < values.length; i++) {
        var val = values[i];
        if (val!=='update') {
          // console.log(val);
          io.emit('new-message', val);
        }
      }
    });

      test--;
      pos=pos+M;
  }

  res.json('ok');


}
}


module.exports = cubeService;
