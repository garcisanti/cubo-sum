var Cube = require('../classes/cube.class');
describe('Cube()', function() {
  it('set cube side', function() {
    var cube = new Cube(5);
    expect(cube.side).toEqual(5);
  });

  it('test init', function() {
    var n = 4;
    var cube = new Cube(n);
    for (var i = 0; i < n; i++) {
      for (var j = 0; j < n; j++) {
        for (var k = 0; k < n; k++) {
          expect(cube.cube[i][j][k]).toEqual(0);
        }
      }
    }
  });

  it('test update', function() {
    var n = 4;
    var cube = new Cube(n);
    for (var i = 0; i < n; i++) {
      for (var j = 0; j < n; j++) {
        for (var k = 0; k < n; k++) {
          cube.update(i,j,k,1);
          expect(cube.cube[i][j][k]).toEqual(1);
        }
      }
    }
  });

  it('test sum', function() {
    var n = 4;
    var cube = new Cube(n);
    expect(cube.sum(0,0,0,3,3,3)).toEqual(0);
    for (var i = 0; i < n; i++) {
      for (var j = 0; j < n; j++) {
        for (var k = 0; k < n; k++) {
          cube.update(i,j,k,1);
        }
      }
    }
    expect(cube.sum(0,0,0,3,3,3)).toEqual(64);
  });

});
