# CubeSum

 ![alt text](/img/cube.png)

## Server

Run node server

## Front end

Run yarn start

## Server tests

Run yarn testserver

## Capas de la aplicación:

### Back end

* **/server/routes/api:** Describe el enrutamiento de peticiones del servidor.
* **/server/services/cube:** Responde a las solicitudes del cliente. En este caso toma toda la entrada del usuario y lo convierte a los casos de prueba en el cubo.
* **/server/controller/cube:** Crea una instancia de un cubo para poder aplicar la lógica sobre el mismo.
* **/server/classes/cube:** Describe la clase cubo con su cuerpo y desfinición de métodos.

### Front end

* **/src/app.module.ts:** Declaracion e inyección de dependencias.
* **/src/app.component.html:** Definicion de la vista.
* **/src/app.component.ts:** Descripción de la logica de la vista.
* **/src/app.service.ts:** Definicion de servicios REST.

## Preguntas
* **¿En qué consiste el principio de responsabilidad única? ¿Cuál es su propósito?**
Consiste en que cada clase, componente, o módulo debe realizar una tarea o grupos de tareas con un propósito semejante.
* **¿Qué características tiene según tu opinión “buen” código o código limpio?**
Un código limpio debe ser un código facil de entender por diferentes programadores, y cercanos o ajenos al código. Debe ser un código altamente desacoplado, que permita ser extendido sin la necesidad de ser modificado, o al menos no modificar gran parte del mismo. También debe ser muy claro y expresivo con los nombres de variables creadas.
